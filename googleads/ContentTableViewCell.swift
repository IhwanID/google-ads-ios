//
//  ContentTableViewCell.swift
//  googleads
//
//  Created by Ihwan ID on 31/08/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit

class ContentTableViewCell: UITableViewCell {

    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    func configure(){
        titleLabel.text = "Hello"
    }

}
