//
//  ViewController.swift
//  googleads
//
//  Created by Ihwan ID on 31/08/20.
//  Copyright © 2020 Ihwan ID. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    struct Constant{
        static let interstisialId = "ca-app-pub-1465397009245921/4720391408"
        static let nativeId = "ca-app-pub-1465397009245921/2300728734"
    }

    private var intersitialAd: GADInterstitial?
    private var nativeAds = [GADUnifiedNativeAd]()
    var adLoader: GADAdLoader!

    var tableViewItems = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        let options = GADMultipleAdsAdLoaderOptions()
        options.numberOfAds = 5
        adLoader = GADAdLoader(adUnitID: Constant.nativeId,
        rootViewController: self,
        adTypes: [.unifiedNative],
        options: [options])
        adLoader.delegate = self
        adLoader.load(GADRequest())
        self.intersitialAd = createAd()
    }

    func createAd() -> GADInterstitial{
        let ad = GADInterstitial(adUnitID: Constant.interstisialId)
        ad.delegate = self
        ad.load(GADRequest())
        return ad
    }

    func addNativeAds() {
//       if nativeAds.count <= 0 {
//         return
//       }
//
//       let adInterval = (10 / nativeAds.count) + 1
//       var index = 0
//       for nativeAd in nativeAds {
//         if index < 5 {
//           tableViewItems.insert(nativeAd, at: index)
//           index += adInterval
//         } else {
//           break
//         }
//       }
     }

    @IBAction func didButtonTapped(_ sender: Any) {
        let x = Int.random(in: 0..<10)
        if x % 2 == 0 {
            if intersitialAd?.isReady == true{
                       intersitialAd?.present(fromRootViewController: self)
        }
        }

    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != 25{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContentCell") as? ContentTableViewCell else {return UITableViewCell()}
            cell.configure()
            return cell
        }else{

           let cell = tableView.dequeueReusableCell(withIdentifier: "AdsCell",for: indexPath)

            let adView : GADUnifiedNativeAdView = cell.contentView.subviews
              .first as! GADUnifiedNativeAdView

            let nativeAd = nativeAds.first as! GADUnifiedNativeAd
            nativeAd.rootViewController = self
            // Associate the ad view with the ad object.
            // This is required to make the ad clickable.
            adView.nativeAd = nativeAd
            (adView.headlineView as! UILabel).text = "\(nativeAd.headline) \n \(nativeAd.advertiser) \n \(nativeAd.callToAction)\n \(nativeAd.body) \n \(nativeAd.price) \n \(nativeAd.store) "
            adView.mediaView?.mediaContent = nativeAd.mediaContent

            return cell
        }
    }

}

extension ViewController: GADInterstitialDelegate{
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        intersitialAd = createAd()
    }
}

extension ViewController: GADUnifiedNativeAdLoaderDelegate{
    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
print("Received native ad: \(nativeAd)")
         nativeAds.append(nativeAd)
    }

    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        print("\(adLoader) failed with error: \(error.localizedDescription)")

    }

    func adLoaderDidFinishLoading(_ adLoader: GADAdLoader) {
addNativeAds()
    }


}
